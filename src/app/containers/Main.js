/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';
import Title from '../components/Title';

const bg = '../assets/images/256327-P4ELIC-585.png';

class Main extends Component {
  render = () => {
    return (
      <View style={styles.layer}>
        <View style={styles.containerBackground}>
          <Image
            source={require(bg)}
            style={styles.background}
            resizeMode="cover"
          />
        </View>
        <Title />
        <View style={styles.containerText}>
          <Text style={styles.text}>Continúa con tus redes: </Text>
        </View>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  containerText: {
    marginTop: 237 - 89,
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerTitle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 89,
  },
  containerBackground: {
    position: 'absolute',
    width: '230%',
  },
  skipButton: {
    backgroundColor: 'black',
    color: 'white',
    marginTop: 10,
    paddingLeft: 30,
    fontSize: 20,
  },
  mainContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  image: {
    width: '100%',
  },
  text: {
    fontSize: 23,
    color: '#ffffff',
    textAlign: 'center',
  },
  title: {
    width: 266.6,
  },
  background: {
    width: '100%',
  },
  layer: {
    backgroundColor: 'rgb(60, 139,109)',
    opacity: 0.6,
    width: '100%',
    height: '100%',
  },
});

export default Main;
