/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {StyleSheet, View, Text, ImageBackground} from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';

const slides = [
  {
    key: '1',
    title: '¿Qué es Llevox?',
    text: 'Encuentra el transporte\nideal para tus mudanzas\ny envíos',
    image: require('../assets/images/onboarding-1.jpg'),
    backgroundColor: '#59b2ab',
  },
  {
    key: '2',
    title: 'Además...',
    text:
      'Puedes pagar online o en\nefectivo de la manera\nmás simple y segura',
    image: require('../assets/images/onboarding-2.jpg'),
    backgroundColor: '#febe29',
  },
  {
    key: '3',
    title: '¿Cómo funciona?',
    text:
      'Completa una solicitud y\nrecibe cotizaciones de los\nmejores profesionales',
    image: require('../assets/images/onboarding-3.jpg'),
    backgroundColor: '#22bcb5',
  },
];

class IntroSlider extends Component {
  state = {
    showRealApp: false,
  };
  // eslint-disable-next-line prettier/prettier
  _renderItem = (item) => {
    return (
      <ImageBackground
        source={item.item.image}
        style={styles.background}
        resizeMode="cover">
        <View style={styles.layer}>
          <Text style={styles.title}>{item.item.title}</Text>
          <Text style={styles.text}>{item.item.text}</Text>
        </View>
      </ImageBackground>
    );
  };
  _onDone = () => {
    this.props.navigation.navigate('Home');
  };
  render() {
    return (
      <AppIntroSlider
        showSkipButton={true}
        renderItem={this._renderItem}
        slides={slides}
        onDone={this._onDone}
      />
    );
  }
}

const styles = StyleSheet.create({
  skipButton: {
    backgroundColor: 'black',
    color: 'white',
    marginTop: 10,
    paddingLeft: 30,
    fontSize: 20,
  },
  mainContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  image: {
    width: '100%',
  },
  text: {
    fontSize: 25,
    color: '#ffffff',
    textAlign: 'center',
    paddingTop: 130,
  },
  title: {
    fontSize: 50,
    color: '#ffffff',
    textAlign: 'center',
    position: 'relative',
    paddingTop: 160,
  },
  background: {
    justifyContent: 'flex-start',
  },
  layer: {
    backgroundColor: 'rgb(60, 139,109)',
    opacity: 0.6,
    width: '100%',
    height: '100%',
  },
});

export default IntroSlider;
