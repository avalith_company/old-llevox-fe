import React, {Component} from 'react';
import {View, Image, StyleSheet} from 'react-native';

const url = '../assets/images/Layer_x0020_1.png';

class Title extends Component {
  render = () => {
    return (
      <View style={styles.containerTitle}>
        <Image style={styles.title} source={require(url)} />
      </View>
    );
  };
}

const styles = StyleSheet.create({
  title: {
    width: 266.6,
  },
  containerTitle: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 89,
    zIndex: 999,
  },
});

export default Title;
