import React, {Component} from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Main from './containers/Main';
import IntroSlider from './containers/IntroSlider';

const MainNavigator = createStackNavigator(
  {
    Home: Main,
    Slider: IntroSlider,
  },
  {
    headerMode: 'none',
    initialRouteName: 'Slider',
  },
);

// test

const AppContainer = createAppContainer(MainNavigator);

export default class App extends Component {
  render() {
    return <AppContainer />;
  }
}
